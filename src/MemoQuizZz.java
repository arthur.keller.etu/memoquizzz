
import extensions.*;
import java.util.regex.Pattern;

class MemoQuizZz extends Program {
    // Secondes vers millisecondes, utile pour initialiser les délais en secondes
    final int STOMS = 1000;
    // Bornes des entiers, utiles pour les menus ne définissant une (ou aucune) borne
    final int INT_MIN = -2147483648, INT_MAX = 2147483647;
    // Droits d'éditions et de manipulation du jeu
    final int DROIT_JOUER = 0x01, DROIT_AJOUTER_QUESTION = 0x02, DROIT_RETIRER_QUESTION = 0x04, DROIT_AJOUTER_CATEGORIE = 0x08, DROIT_RETIRER_CATEGORIE = 0x10;
	
    void algorithm() {
    
		clearScreen();
		afficherLogo();
	
		Categorie[] categories = chargerCategories();
		Profil[] profilsCharges = chargerProfils();
	
		if (boolChoix('J', 'E', "Veuillez choisir un mode :\n\tJ: Joueur\n\tE: Éditeur")) { // mode Joueur
		    afficherMode("Joueur");
		    int quantite = menuChoix(1, 4, "Veuillez choisir le nombre de joueurs (entre 1 et 4)");
		    Profil[] profils = entreeUtilisateurs(quantite, profilsCharges);

		    int niveau = menuChoix(1, 3, "Veuillez entrer le niveau de difficulté désiré : facile (1), normal (2), difficile (3)");
		    Question[] questions = chargerQuestions(categories);
			
		    int nombreTours = menuChoix(1, INT_MAX, "Veuillez choisir le nombre de tours désirés (au minimum 1 tour)");	    

		    Profil actuel = profils[0];
	    	int tours = 1;
	    	boolean tourne = true;

	    	while (tourne) {
				int choix = menuChoix(1, 4, "Que désirez vous faire ?\n\t1: Jouer\n\t2: Consulter les scores\n\t3: Gérer votre compte\n\t4: Quitter");
				if (choix == 1) {
				    // TODO boucle de jeu, actuellement limitée à 1 seul tour
				    do {
						println("Tour n°"+tours);
						if (poserQuestion(questions)) {
						    println("Bravo");
						    actuel.scorePartie += 1;
						    actuel.score += 1;
						} else {
						    println("Échec");
						}
						tours+=1;
				    } while (actuel.scorePartie != 20 && tours < 1);
				    afficherScores(profils, "Score de fin de partie", false);
				} else if (choix == 2) {
				    afficherScores(profilsCharges, "Scores généraux", true);
				} else if (choix == 3) {
				    // TODO Paramétrage du compte
				} else {
				    tourne = false;
				}
	    	}
	    	profilsCharges = fusionner(profilsCharges, profils);
		}
		enregistrerProfils(profilsCharges);
    }

    void afficherMode(String mode) {
		clearScreen();
		cursor(0,0);
		afficherASCIIArt(mode);
    }

    void afficherLogo() {
		afficherASCIIArt("M-QUIZZZ");
		println("Appuyez sur Entrée");
		readString();
		clearScreen();
		cursor(0,0);
    }

    void afficherScores(Profil[] profils, String titre, boolean general) {
		clearScreen();
		cursor(0,0);
		afficherASCIIArt(titre);
		int idx = 1;
		String s;

		Profil[] topJoueurs = trierScores(profils, 10, general);

		for (Profil p : topJoueurs) {
			if (general) {
				println("" + idx++ + ": " + p.nom + " a " + p.score + " points");
			} else {
				println("J" + idx++ + ": " + p.nom + " a " + p.scorePartie + " points");
			}	
		}
    }
	
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //	GESTIONS DES DONNEES PERSISTANTES

    // TODO chargement et enregistrement des profils en local (à faire)
    Profil[] chargerProfils() {
	CSVFile csv = loadCSV("profils.csv", ';');
	final int LIMIT = rowCount(csv);
	final int COL_NOM = 0, COL_DROITS = 1, COL_SCORE = 2, COL_JOUEES = 3, COL_GAGNEES = 4, COL_MAIL1 = 5, COL_MAIL2 = 6, COL_MDP1 = 7, COL_MDP2 = 8;
	int idx = 0;
		
	Profil[] profils = new Profil[LIMIT -1];
	Profil p;
	for (int i = 1; i < LIMIT; i += 1) {
	    p = new Profil();
	    p.nom = getCell(csv, i, COL_NOM);
	    p.mdp = longsToBytes(Long.parseLong(getCell(csv, i, COL_MDP1)), Long.parseLong(getCell(csv, i, COL_MDP2)));
	    p.mail = longsToBytes(Long.parseLong(getCell(csv, i, COL_MAIL1)), Long.parseLong(getCell(csv, i, COL_MAIL2)));
	    p.droits = stringToInt(getCell(csv, i, COL_DROITS));
	    p.score = stringToInt(getCell(csv, i, COL_SCORE));
	    p.nbPartiesJouees = stringToInt(getCell(csv, i, COL_JOUEES));
	    p.nbPartiesGagnees = stringToInt(getCell(csv, i, COL_GAGNEES));
	    profils[idx++] = p;
	}
	return profils;
    }
	
    void afficher(String[][] tab) {
	println("lengths tab=" + length(tab) + " tab[i]=" + length(tab[0]));
	for (int i = 0; i < length(tab); i++) {
	    print(i + "|");
	    for (int j = 0; j < length(tab[i]); j++) {
		print(tab[i][j] + " ");
	    }
	    println();
	}
	println("fini");
    }
	
    Profil[] fusionner(Profil[] p1, Profil[] p2) {
	Profil[] temporaire = new Profil[length(p1)+length(p2)], resultat;
	int pos = 0;
	for (int i = 0; i < length(p1); i ++) {
	    if (trouverProfil(p1[i].nom, temporaire) == null) {
		temporaire[pos++] = p1[i];
	    }
	}
	for (int i = 0; i < length(p2); i ++) {
	    if (trouverProfil(p2[i].nom, temporaire) == null) {
		temporaire[pos++] = p2[i];
	    }
	}
	resultat = new Profil[pos];
	for (int i = 0; i < pos; i++) {
	    resultat[i] = temporaire[i];
	}
	return resultat;
    }
	
    void enregistrerProfils(Profil[] profils) {
	// nombre de profils + 1 (en-tête), par le nombre de propriétés
	String[][] contenu = new String[length(profils)+1][6];
	contenu[0] = new String[]{"Nom", "Droits", "Score", "NbPartiesJouees","NbPartiesGagnees", "Mail1", "Mail2", "Mdp1", "Mdp2"};
	Profil p;
		
	for (int i = 0; i < length(profils); i++) {
	    p = profils[i];
	    long[] mail = bytesToLongs(p.mail);
	    long[] mdp = bytesToLongs(p.mdp);
	    contenu[i+1] = new String[]{p.nom, ""+p.droits, ""+p.score, ""+p.nbPartiesJouees, ""+p.nbPartiesGagnees,""+mail[0], ""+mail[1], ""+mdp[0], ""+mdp[1]};
	}
	//	afficher(contenu);
	saveCSV(contenu, "../ressources/profils.csv", ';');
	saveCSV(contenu, "profils.csv", ';');
    }

    Categorie[] chargerCategories() {
	CSVFile csv = chargerRessourceCSV("categorie-base.csv");
	final int LIMIT = rowCount(csv);
	final int COL_NOM = 0, COL_CHEMIN = 1; 
	int idx = 0;

	Categorie[] categories = new Categorie[LIMIT - 1];
	Categorie c;
	for (int i = 1; i < LIMIT; i -=- 1) {
	    c = new Categorie();
	    c.nom = getCell(csv, i, COL_NOM);
	    c.chemin = getCell(csv, i, COL_CHEMIN);
	    c.questions = chargerQuestions(c.chemin, c.nom);
	    categories[idx++] = c;
	}
	return categories;
    }

    Question[] chargerQuestions(String chemin, String matiere) {
	CSVFile csv = chargerRessourceCSV(chemin);
	final int LIMIT = rowCount(csv);
	final int COL_TEXTE = 0, COL_CARTE = 1, COL_NB_REP = 2, COL_NB_VREP = 3, COL_REPS = 4;
	int idx = 0, jdx;

	Question[] questions = new Question[LIMIT -1];
	Question q;
	Reponse r;
	for (int i = 1; i < LIMIT; i++) {
	    q = new Question();
	    q.matiere = matiere;
			
	    q.carte = chargerRessource(getCell(csv, i, COL_CARTE));
	    q.question = getCell(csv,i, COL_TEXTE);
	    int nbReps = stringToInt(getCell(csv, i, COL_NB_REP)), nbVRep = stringToInt(getCell(csv,i, COL_NB_VREP));
	    q.reponses = new Reponse[nbReps];
	    jdx = 0;
	    for (int j = COL_REPS; j < nbReps+COL_REPS; j++) {
		r = new Reponse();
		r.texte = getCell(csv, i,j);
		r.valide = nbVRep-- > 0;
		q.reponses[jdx++] = r;
	    }
	    questions[idx++] = q;
	}
	return questions;
    }

    Question[] chargerQuestions(Categorie[] categories) {
	int size = 0, idx = 0;
	for (int i = 0; i < length(categories); i ++) {
	    size += length(categories[i].questions);
	}
	Question[] questions = new Question[size];
	for (int i = 0; i < length(categories); i++) {
	    for (int j = 0; j < length(categories[i].questions); j++) {
		questions[idx++] = categories[i].questions[j];
	    }
	}
	return questions;
    }
	
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //	GESTIONS DES QUESTIONS
	
    Reponse[] melanger(Reponse[] reponses) {
	Reponse[] reponsesMelangees = new Reponse[length(reponses)];
	int pos = 0, random;
		
	while (pos < length(reponses)) {
	    random = (int) (random() * length(reponses));
	    if (!reponses[random].melangee) {
		reponsesMelangees[pos++] = reponses[random];
		reponses[random].melangee = true;
	    }
	}
	return reponsesMelangees;
    }
	
    void afficherQuestion(Question q, Reponse[] reponses) {
	clearScreen();
	cursor(0,0);
	afficherASCIIArt(q.matiere);
	imprimerFichier(q.carte);
	delay(5*STOMS);
	clearScreen();
	println("Question: \""+q.question+"\"");
		
	for (int i = 0; i < length(reponses); i++) {
	    println((i+1) + ": " + reponses[i].texte); 
	}
    }

    boolean poserQuestion(Question[] questions) {
	int aleaQuestion;
	Question q;
		
	do {
	    aleaQuestion = (int) (random() * length(questions));
	    q = questions[aleaQuestion];
	} while (q.posee);
	
	Reponse[] reponsesMelangees = melanger(q.reponses);
	afficherQuestion(q, reponsesMelangees);
		
	print("Entrez votre réponse: ");
	return reponsesMelangees[readInt()-1].valide;
    }
	
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //	GESTIONS DES PROFILS

    void afficherProfils(Profil[] profils, String prefix) {
		for (int i = 0; i < length(profils); i ++) {
	    	println(prefix+"J"+(i+1)+": " + profils[i].nom + ", score: " + profils[i].score );
		}
    }
	
    Profil trouverProfil(String nom, Profil[] profils) {
		int idx = 0;
		while (idx < length(profils) && profils[idx] != null) {
		    if (equals(profils[idx].nom, nom)) {
				return profils[idx];
		    }
		    idx++;
		}
		return null;
    }
    
    boolean reinitialiserMdp(Profil profil) {
    	return true;
    }
	
    boolean demandeMdp(Profil profil, String message) {
		String entree;
		int tentatives = 0;
		boolean valide;
		print(message);
		do {
		    entree = readString();
		    valide =  ( (estVide(profil.mdp) && equals(entree, "")) || equals(md5(strToBytes(profil.nom + entree)), profil.mdp) );
		    
		    if (!valide) {
				if (tentatives < 2) {
				    print("Mauvais mot de passe ! Veuillez le resaisir : ");
					tentatives++;
				} else {
					if (boolChoix('y', 'n', "Voulez-vous réinitialiser votre mot de passe ? (y|n)")) {
						if (reinitialiserMdp(profil)) {							
							valide = demandeMdp(profil, "Entrez votre nouveau mot de passe : ");
						} else {
							println("Une erreur est survenue lors de la réinitialisation de votre mot de passe");
						}
					}
				}
		    }
		} while(!valide);
		return valide;
    }
	
    Profil rechercheParPseudo(String nom, Profil[] profils) {
	int idx = 0;
	Profil profil = trouverProfil(nom, profils);
		
	while (profil == null && idx < length(profils) && profils[idx] != null) {
	    double probabilite = jaroWinklerDistance(nom, profils[idx].nom, length(nom)%4);
	    if (probabilite >= 0.9 && boolChoix('y', 'n', "Est-ce votre pseudo ? " + profils[idx].nom + " (y|n)")) {
		profil = profils[idx];
	    }
	    idx++;
	}
	return profil;
    }
	
    Profil authentification(Profil[] profils) {
		println("*** Authentification ***");
		String entree;
		Profil profil = null;
		
		// Recherche du pseudo
		do {
		    print("Entrez votre pseudo : ");
		    entree = readString();
		    profil = rechercheParPseudo(entree, profils);
		    if (profil == null && boolChoix('y', 'n', "Il semble que votre pseudo n'est pas enregistré, souhaitez-vous créer un nouveau profil ? (y|n)")) {
				profil = enregistrerNouveauProfil(entree);
		    } else {
			
		    }
		} while (profil == null);
		
		// Demande du mot de passe
		if (demandeMdp(profil, "Entrez votre mot de passe : ")) {
		    println("*** Authentification réussie ***");
		} else {
		    println("*** Authentification échouée ***");
		    profil = null;
		}
		return profil;
    }
	
    Profil enregistrerNouveauProfil(String nom) {
		Profil profil = newProfil(nom);
		if (boolChoix('y', 'n', "Désirez-vous enregistrer une adresse mail afin de pouvoir réinitialiser votre mot de passe plus tard ?")) {
	    	profil.mail = md5(strToBytes(saisirMail()));
		} else {
		    profil.mail = longsToBytes(0L, 0L);
		}
	
		print("Veuillez ajouter un mot de passe pour finaliser la création de votre profil : ");
		profil.mdp = md5(strToBytes(nom + readString()));
				
		if (demandeMdp(profil, "Veuillez le donner à nouveau : ")) {
		    println("Profil créé !");
	    	profil.nouveau = true;
		} else {
		    println("Échec de la création du profil");
		    profil = null;
		}
		return profil;
    }
	
    Profil entreeUtilisateur(Profil[] profilsCharges, int indice) {
	Profil profil = null;
	println("Authentification du joueur n°" + (indice+1));
	do {
	    profil = authentification(profilsCharges);
	    // échec de l'authentification
	    if (profil == null || !aDroit(profil, DROIT_JOUER)) {
		println("L'authentification du joueur n°" + (indice+1) + " a échoué");
	    } else {
		if (profil.nouveau) {
		    println("Bienvenue parmi nous " + profil.nom + " !");					
		} else {
		    println("Bon retour parmi nous " + profil.nom + " !");
		}
	    }
	} while (profil == null || !aDroit(profil, DROIT_JOUER));
	return profil;
    }

    Profil[] entreeUtilisateurs(int quantite, Profil[] profilsCharges) {
	Profil[] profils = new Profil[quantite];
	for (int i = 0; i < quantite; i -=- 1) {
	    profils[i] = entreeUtilisateur(profilsCharges, i);
	}
	return profils;
    }
	
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //	PSEUDO-CONSTRUCTEURS

    Profil newProfil(String nom) {
	return newProfil(nom, DROIT_JOUER);
    }
	
    Profil newProfil(String nom, int droits) {
	Profil j = new Profil();
	j.nom = nom;
	j.droits = droits;
	return j;
    }

    Partie newPartie(Profil[] profils, Categorie[] categories, Question[] questions, int difficulte) {
	Partie p = new Partie();
	p.profils = profils;
	p.categories = categories;
	p.questions = questions;
	p.difficulte = difficulte;
	return p;
    }

    Categorie newCategorie(String nom, String chemin, Question[] questions) {
	Categorie c = new Categorie();
	c.nom = nom;
	c.chemin = chemin;
	c.questions = questions;
	return c;
    }

    Question newQuestion(String categorie, File carte, int difficulte, String question, Reponse[] reponses) {
	Question q = new Question();
	q.matiere = categorie;
	q.carte = carte;
	q.difficulte = difficulte;
	q.question = question;
	q.reponses = reponses;
	return q;
    }

    Reponse newReponse(String texte, boolean valide) {
	Reponse r = new Reponse();
	r.texte = texte;
	r.valide = valide;
	return r;
    }
    
    QuicksortElement newElement(int indiceDansTableau, int valeur) {
    	QuicksortElement e = new QuicksortElement();
    	e.indiceDansTableau = indiceDansTableau;
    	e.valeur = valeur;
    	return e;
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  FONCTIONS À PROPOS DES MAILS

    String saisirMail() {
		String pattern = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$", entree;
		Pattern p = Pattern.compile(pattern);
		boolean valide;
		print("Veuillez entrer votre adresse mail : ");
		do {
		    entree = readString();
		    valide = (p.matcher(entree).matches());
		    if (!valide) {
		    	print("Adresse mail incompréhensible, veuillez la resaisir : ");
		    }
		} while(!valide);
		return entree;
    }
    
    void envoyerMail(String destination, String objet, String mail, String message, String erreur) {
    	Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable","true");
        props.put("mail.smtp.ssl.protocols", "TLSv1.2");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("memoquizzz@gmail.com", "gchicwvhuiktzpfr");
            }
        });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("memoquizzz@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(destination));
            message.setSubject(objet);
            message.setContent(mail, "text/html");	
            Transport.send(message);
            System.out.println(message);
        } catch (MessagingException e) {
        	System.out.println(erreur);
            throw new RuntimeException(e);
        }
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //	FONCTIONS UTILES
    
	int indexOf(String s, char c) {
		int idx = 0;
		while (idx < length(s) && c != charAt(s, idx++));
		return ((c == charAt(s, --idx)) ? idx : -1 );
	}
	
	int lastIndexOf(String s, char c) {
		int idx = length(s) - 1;
		while (idx >= 0 && c != charAt(s, idx--));
		return ((c == charAt(s, ++idx)) ? idx : -1);
	}

    Profil[] trierScores(Profil[] profils, int limit, boolean general) {
    	final int LIMIT = min(length(profils), limit);
   		Profil[] profilsTries = new Profil[LIMIT];
    	QuicksortElement[] elements = new QuicksortElement[LIMIT];
    	for (int i = 0; i < LIMIT; i++) {
    		elements[i] = newElement(i, ((general) ? profils[i].score : profils[i].scorePartie));
    	}
		quicksort(elements, 0, length(elements)-1);
		for (int i = 0; i < LIMIT; i++) {
    		profilsTries[i] = profils[elements[i].indiceDansTableau];
    	}
		return profilsTries;
    }

    int partitionner(QuicksortElement[] elements, int premier, int dernier) {
		int pivot = elements[dernier].valeur, i = premier - 1;
		QuicksortElement tmp;
		
		for (int j = premier; j < dernier; j++) {
		    if (elements[j].valeur < pivot) {
				i++;
				tmp = elements[i];
				elements[i] = elements[j];
				elements[j] = tmp;
		    }
		}
		tmp = elements[++i];
		elements[i] = elements[dernier];
		elements[dernier] = tmp;
		return i;
    }
    
    void quicksort(QuicksortElement[] elements, int premier, int dernier) {
		if (premier < dernier) {
	    	int pivot = partitionner(elements, premier, dernier);
	    	quicksort(elements, premier, pivot-1);
	    	quicksort(elements, pivot+1, dernier);
		}
    }
    
    byte[] longToBytes(long value) {
	return new byte[] {(byte)(value>>>56), (byte)(value >>> 48), (byte)(value >>> 40), (byte)(value >>> 32), (byte)(value>>>24), (byte)(value>>>16), (byte)(value>>>8), (byte) value};
    }
    
    byte[] longsToBytes(long l1, long l2) {
		byte[] r = new byte[16], tmp;
		int idx = 0;
	
		tmp = longToBytes(l1);
		for (int i = 0; i < length(tmp); i++) {
		    r[idx++] = tmp[i];
		}

		tmp = longToBytes(l2);
		for (int i = 0; i < length(tmp); i++) {
		    r[idx++] = tmp[i];
		}
		return r;
    }
    
    long[] bytesToLongs(byte[] ba) {
		long[] r = new long[length(ba)/8];
		long l = 0L;
		int idx = 0;
		for (int i = 0; i < length(ba); i++) {
		    if (i != 0 && i % 8 == 0) {
				r[idx++] = l;
				l = (ba[i] & 0xff);
		    } else {
				l <<= 8;
				l += (ba[i] & 0xff);
		    }
		}
		r[idx] = l;
		return r;
    }

    boolean equals(byte[] b1, byte[] b2) {
		boolean equals = true;
		int idx = 0;
		if (length(b1) != length(b2)) return false;
	
		while (idx < length(b1) && equals) {
		    equals = (b1[idx]==b2[idx++]);
		}
		return equals;
    }
    
    String toString(byte[] ba, String charset) {
	String s = "";
	int idx = 0;

	if (equals(charset, "UTF-8")) {
	    while (idx < length(ba)) {
		int c = ba[idx++] & 0xff;
		if (c <= 0x7f) {
		    s += (char) c;
		} else if ((c & 0xe0) == 0xc0) {
		    int c2 = ba[idx++] & 0xff;
		    s += (char) ((c & 0x1f) << 6 | (c2 & 0x3f));
		} else if ((c & 0xf0) == 0xe0){
		    int c2 = ba[idx++] & 0xff, c3 = ba[idx++] & 0xff;
		    s+= (char) ((c & 0x0f) << 12 | (c2 & 0x3f) << 6 | (c3 & 0x3f));
		} else if ((c & 0xf8) == 0xf0) {
		    int c2 = ba[idx++] & 0xff, c3 = ba[idx++] & 0xff, c4 = ba[idx++] & 0xff;		
		    int codepoint = (c & 0x07) << 18 | (c2 & 0x3f) << 12 | (c3 & 0x3f) << 6 | (c4 & 0x3f);
		    int high = (codepoint - 0x010000) / 0x400 + 0xd800;
		    int low = (codepoint - 0x010000) % 0x400 + 0xdc00;
		    s+= (char) high + "" + (char) low; 
		}
	    }
	} else if (equals(charset, "ISO-8859-2")) {
	    while (idx < length(ba)) {
		char c = (char) ba[idx++];
		s += c;
	    }
	}
	return s;
    }
	
    byte[] strToBytes(String s) {
	byte[] tmp = new byte[length(s) * 4], r;
	int pos = 0;

	if (length(s) == 0) {
	    return new byte[]{0x0};
	}
		
	for (int i = 0; i < length(s); i ++) {
	    long c = (long) charAt(s, i);
			
	    final boolean est4b = ( (c & 0xdc00) == 0xd800 || (c & 0xdc00) == 0xdc00);
			
	    if (c <= 0x7f && !est4b ) { // ASCII CHARS (0x0 -> 0x7f)
		tmp[pos++] = (byte) c;
	    } else if (c <= 0x7ff && !est4b ) { // 2 bytes UTF-8 (0x80 -> 0x7ff)
		tmp[pos++] = (byte) ((c >> 6) | 0xc0);
		tmp[pos++] = (byte) ((c & 0x3f) | 0x80);
	    } else if (c <= 0xffff && !est4b) { // 3 bytes UTF-8 (0x800 -> 0xffff)
		tmp[pos++] = (byte) ((c >> 12) | 0xe0);
		tmp[pos++] = (byte) (((c >> 6) & 0x3f) | 0x80);
		tmp[pos++] = (byte) ((c & 0x3f) | 0x80);
	    } else { // 4 bytes UTF-8 (0x10000 -> 0x1fffff)
		long codepoint = 0x010000 | (c & 0x03ff) << 10 | (((long) charAt(s, ++i)) & 0x03ff);
		tmp[pos++] = (byte) ((codepoint >> 18) | 0xf0);
		tmp[pos++] = (byte) (((codepoint >> 12) & 0x3f) | 0x80);
		tmp[pos++] = (byte) (((codepoint >> 6) & 0x3f) | 0x80);
		tmp[pos++] = (byte) ((codepoint & 0x3f) | 0x80);				
	    }
	}
	r = new byte[pos];
	for (int i = 0; i < pos; i ++) {
	    r[i] = tmp[i];
	}
	return r;
    }
		
    int hashCode(String str) {
	int hcode = 0;
	for (int i = 0; i < length(str); i++) {
	    hcode += charAt(str, i) * pow(31, length(str) - i - 1);
	}
	return hcode;
    }

    int rotateLeft(int x, int n) {
	return ((x << n) | (x >>> (32-n))); 
    }
    
    String toHex(byte[] ba) {
	String s = "";
	for (byte b: ba) {
	    s+=String.format("%02x", b);
	}
	return s;
    }

    boolean estVide(byte[] ba) {
	int idx = 0;
	boolean vide = true;

	while (idx < length(ba) && vide) {
	    vide = (ba[idx++] == (byte) 0x0);
	}
	return vide;
    }
    
    byte[] md5(byte[] message) {
	final int INIT_A = 0x67452301, INIT_B = (int)0xEFCDAB89L, INIT_C = (int)0x98BADCFEL, INIT_D = 0x10325476;
	final int[] SHIFT_AMTS = {
	    7, 12, 17, 22,
	    5,  9, 14, 20,
	    4, 11, 16, 23,
	    6, 10, 15, 21
	};
	final int[] TABLE_T = { 0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee, 0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
	    0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be, 0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
	    0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa, 0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
	    0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed, 0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
	    0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c, 0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
	    0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05, 0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
	    0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039, 0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
	    0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1, 0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391 };
	
	int messageLenBytes = length(message);
	int numBlocks = ((messageLenBytes + 8) >>> 6) + 1;
	int totalLen = numBlocks << 6;
	byte[] paddingBytes = new byte[totalLen - messageLenBytes];
	paddingBytes[0] = (byte)0x80;

	long messageLenBits = (long)messageLenBytes << 3;
	for (int i = 0; i < 8; i++) {
	    paddingBytes[length(paddingBytes) - 8 + i] = (byte)messageLenBits;
	    messageLenBits >>>= 8;
	}

	int a = INIT_A;
	int b = INIT_B;
	int c = INIT_C;
	int d = INIT_D;
	int[] buffer = new int[16];
	for (int i = 0; i < numBlocks; i ++) {
	    int index = i << 6;
	    for (int j = 0; j < 64; j++, index++) {
		buffer[j >>> 2] = ((int)((index < messageLenBytes) ? message[index] : paddingBytes[index - messageLenBytes]) << 24) | (buffer[j >>> 2] >>> 8);
	    }
	    int originalA = a;
	    int originalB = b;
	    int originalC = c;
	    int originalD = d;
	    for (int j = 0; j < 64; j++) {
		int div16 = j >>> 4;
		int f = 0;
		int bufferIndex = j;
		switch (div16) {
		case 0:
		    f = (b & c) | (~b & d);
		    break;
				
		case 1:
		    f = (b & d) | (c & ~d);
		    bufferIndex = (bufferIndex * 5 + 1) & 0x0F;
		    break;
				
		case 2:
		    f = b ^ c ^ d;
		    bufferIndex = (bufferIndex * 3 + 5) & 0x0F;
		    break;
				
		case 3:
		    f = c ^ (b | ~d);
		    bufferIndex = (bufferIndex * 7) & 0x0F;
		    break;
		}
		int temp = b + rotateLeft(a + f + buffer[bufferIndex] + TABLE_T[j], SHIFT_AMTS[(div16 << 2) | (j & 3)]);
		a = d;
		d = c;
		c = b;
		b = temp;
	    }
		  
	    a += originalA;
	    b += originalB;
	    c += originalC;
	    d += originalD;
	}

	byte[] md5 = new byte[16];
	int count = 0;
	for (int i = 0; i < 4; i++) {
	    int n = (i == 0) ? a : ((i == 1) ? b : ((i == 2) ? c : d));
	    for (int j = 0; j < 4; j++) {
		md5[count++] = (byte)n;
		n >>>= 8;
	    }
	}
	return md5;
    }
	
    boolean boolChoix(char v, char f, String question) {
	String entree;
	int choix;
	println(question);
	print("Votre choix: ");
	do {
	    entree = readString();
	    if (equals(entree, "1") || equals(entree, "" + v) || equals(entree, "")) {
		choix = 1;
	    } else if (equals(entree, "2") || equals(entree, "" +f)) {
		choix = 0;
	    } else {
		print("Entrée invalide ! Veuillez la resaisir : ");				
		choix = -1;
	    }
	} while (choix == -1);
	return (choix == 1) ? true : false;
    }
	
    // En cas de besoin les bornes maximales et minimales de l'Integer Java sont disponibles sous forme de constantes	
    int menuChoix(int min, int max, String question) {
	int entree;
	boolean valide = false;
	println(question);
	print("Votre choix: ");
	do {
	    entree = readInt();
	    valide = ( entree >= min && entree <= max );
	    if (!valide) {
		print("Entrée invalide ! Veuillez la resaisir : ");
	    }
	} while(!valide);
	return entree;
    }

    boolean aDroit(Profil profil, int droit) {
	return ((profil.droits & droit) == droit);
    }

    void afficherASCIIArt(String text) {
	text = toLowerCase(text);
	File[] lettres = new File[length(text)];
	for (int i = 0; i < length(text); i++) {
	    lettres[i] = chargerRessource("/lettres/letter-" + charAt(text,i) + ".txt");
	}
	
	final int SIZE = 6;
	for (int i = 0; i < SIZE; i++) {
	    for (int j = 0; j < length(lettres); j++) {
		print(readLine(lettres[j]));
	    }
	    println();
	}
    }
	
    double jaroDistance(String s1, String s2) {
	final double TIERS = 3.0;
	int len1 = length(s1), len2 = length(s2), maxDist = floor(max(len1,len2)/2)-1, match = 0, point = 0;
	int[] hash1 = new int[len1], hash2 = new int[len2];
	double t = 0.0;

	if (s1 == s2 || equals(s1, s2)) return 1.0;
			
	if (len1 == 0 || len2 == 0) return 0.0;
		
	for (int i = 0; i < len1; i++) {
	    int j = max(0, i - maxDist);
	    while (j < min(len2, i + maxDist + 1) && !(charAt(s1, i) == charAt(s2, j) && hash2[j] == 0) ) j++;
			
	    if (j < min(len2, i + maxDist + 1) ) {
		hash1[i] = 1;
		hash2[j] = 1;
		match++;
	    }
	}
	if (match==0) return 0.0;
		
	for (int i = 0; i < len1; i++) {
	    if (hash1[i] == 1) {
		while (hash2[point] == 0) {
		    point++;
		}
		if (charAt(s1, i) != charAt(s2, point++)) {
		    t++;
		}
	    }
	}
	t/=2;
	return (((double) match) / ((double)len1) + ((double) match) / ((double)len2) + ((double)match-t) / ((double)match)) / TIERS;
    }
	
    double jaroWinklerDistance(String s1, String s2, int prefixeCommun) {
	final double COEFFICIENT = 0.1, SEUIL = 0.7;
		
	double jaroDist = jaroDistance(s1, s2);
	if (jaroDist > SEUIL) {
	    int prefix = 0, i = 0;
	    while(i < min(length(s1), length(s2)) && charAt(s1, i) == charAt(s2, i)) {
		i++; 
		prefix++;
	    }
			
	    prefix=min(prefixeCommun, prefix);
			
	    jaroDist+=COEFFICIENT*prefix*(1-jaroDist);
	}
	return jaroDist;
    }
		
    File chargerRessource(String nom) {
	return newFile("ressources/"+nom);
    }

    CSVFile chargerRessourceCSV(String nom) {
	return loadCSV("ressources/"+nom, ';');
    }
	
    boolean mailDejaEntre(byte[] mail, Profil profils[]) {
		boolean entered = false;
		int idx = 0;
		while (idx < length(profils) && !entered) {
			entered = equals(mail, profils[idx++].mail);
		}
		return entered;
    }
	
    void imprimerFichier(File file) {
	while (file.ready()) {
	    println(file.readLine());
	}
    }
	
    int floor(double a) {
	if (a >= 0.0) {
	    return (int) a;
	} else {
	    int b = (int)a;
	    return (int) (a==b ? b : b-1);
	}
    }
	
    int max(int a, int b) {
	return (a > b) ? a : b;
    }
	
    int min(int a, int b) {
	return (a < b) ? a : b;
    }

    boolean estNumerique(char c) {
	return c >= 0 && c <= 9;
    }
}
